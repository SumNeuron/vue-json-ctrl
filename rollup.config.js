// rollup.config.js
import vue from 'rollup-plugin-vue';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';

import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';





const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/entry.js',

  output: {
    name: 'JsonCtrl',
    exports: 'named',
    // globals: {
    //   lodash: 'lodash'
    // },
  },
  plugins: [
    vue({
      css: true,
      compileTemplate: true,
    }),
    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
      runtimeHelpers: true,
      plugins: [
        [
          'wildcard',
          {
            exts: ['vue'],
            nostrip: true,
          },
        ],
        '@babel/plugin-external-helpers',
      ],
      presets: [
        [
          '@babel/preset-env',
          {
            modules: false,
          },
        ],
      ],
    }),

    nodeResolve({
      jsnext: true,
      main: true
    }),

    commonjs({
      // non-CommonJS modules will be ignored, but you can also
      // specifically include/exclude files
      include: 'node_modules/**',  // Default: undefined

      // search for files other than .js files (must already
      // be transpiled by a previous plugin!)
      extensions: [ '.js', '.coffee' ],  // Default: [ '.js' ]


      namedExports: {
        './src/store/module/store.js': ['module' ] ,
        'node_modules/lodash/lodash.js': ['debounce'],

      },  // Default: undefined

      // sometimes you have to leave require statements
      // unconverted. Pass an array containing the IDs
      // or a `id => boolean` function. Only use this
      // option if you know what you're doing!
      ignore: [ 'conditional-runtime-dependency' ]
    })
  ],
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
