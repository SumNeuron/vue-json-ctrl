import Vue from 'vue';

// import Vuetify, {
//   VApp, // required
//   VTextField,
//   VCombobox,
//   VFlex,
//   VLayout
// } from 'vuetify/lib'


import module from './store/module/store.js';

import JsonCtrl from './components/JsonCtrl.vue';
import TimSort from './components/TimSort.vue';
import RegEx from './components/RegEx.vue';
import InputAlpha from './components/InputAlpha.vue';
import StoreOptions from './components/StoreOptions.vue'
import CNFView from './components/CNFView.vue'
import CNFViewer from './components/CNFViewer.vue'

import DataVue from './components/DataVue/DataVue.vue';
import DataVueCell from './components/DataVue/DataVueCell.vue';
import DataVueFooter from './components/DataVue/DataVueFooter.vue';
import DataVueFooterCell from './components/DataVue/DataVueFooterCell.vue';
import DataVueHeader from './components/DataVue/DataVueHeader.vue';
import DataVueHeaderCell from './components/DataVue/DataVueHeaderCell.vue';
import DataVuePagination from './components/DataVue/DataVuePagination.vue';
import DataVueRecord from './components/DataVue/DataVueRecord.vue';
import DataVueRecordCell from './components/DataVue/DataVueRecordCell.vue';
import DataVueRecords from './components/DataVue/DataVueRecords.vue';


const components = {
  JsonCtrl, TimSort, RegEx, InputAlpha, CNFView, StoreOptions, CNFViewer,
  DataVue,
  DataVueCell,
  DataVueFooter,
  DataVueFooterCell,
  DataVueHeader,
  DataVueHeaderCell,
  DataVuePagination,
  DataVueRecord,
  DataVueRecordCell,
  DataVueRecords,
}

// const vuetifyComponents = {
//   VApp, // required
//   VTextField,
//   VCombobox,
//   VFlex,
//   VLayout
// }

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  // Object.keys(vuetifyComponents).forEach(name => {
  //   Vue.component(name, vuetifyComponents[name])
  // });

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export default components
export {module}
export {
  JsonCtrl, TimSort, RegEx, InputAlpha, CNFView, StoreOptions, CNFViewer,
  DataVue,
  DataVueCell,
  DataVueFooter,
  DataVueFooterCell,
  DataVueHeader,
  DataVueHeaderCell,
  DataVuePagination,
  DataVueRecord,
  DataVueRecordCell,
  DataVueRecords,
}
