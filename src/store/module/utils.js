import jsoncnf from 'json-cnf';

export function hasSelectedValidOption(selected, options) {
  return Array.isArray(options)
    ? options.indexOf(selected) > -1
    : selected in options;
}


export function hasProvidedValidFilter(filter, fields, config=jsoncnf.config) {
  return [
    hasSelectedValidOption(filter.logic, config.logic),
    hasSelectedValidOption(filter.field, fields),
    hasSelectedValidOption(filter.function, config.functions),
    hasSelectedValidOption(filter.conditional, config.conditionals),
    filter.input !== ''
  ].every(x => x === true);
}


const identity = (id, field, record) => record[field]
const defaultFieldRenderFunction = identity
const defaultFieldSortByFunction = identity
const defaultFieldFilterFunction = identity
const lookup = (object, key, fallback) => key in object ? object[key] : fallback

export {identity, defaultFieldRenderFunction, defaultFieldSortByFunction, defaultFieldFilterFunction, lookup}
