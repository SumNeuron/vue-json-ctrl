import jsontkn from 'json-tkn';
import {hasProvidedValidFilter} from './utils'

const actions = {
  toggleSortSpecification({commit, state}, field) {
    let sortSpecs = state.sortSpecifications
    let sortSpec = {field: field, isAscending: true}

    let found = false
    let i = 0
    for (i = 0; i < sortSpecs.length; i++) {
      let cur = sortSpecs[i]
      if (cur.field === field) {
        if (cur.isAscending) {
          sortSpec.isAscending = false
          commit('setSortSpecification', {index: i, sortSpecification: sortSpec})
          found = true
          break
        } else {
          commit('spliceSortSpecification', i)
          found = true
          break
        }
      }
    }
    if (!found) {
      commit('pushSortSpecification', sortSpec);
    }
  },

  toggleSelectAll({state, getters, commit}, all) {
    let i
    if (all === true) {
      getters.paginated.forEach(id => {
        i = state.selected.indexOf(id)
        if (i < 0) commit('pushSelected', id)
      })
    } else {
      getters.paginated.forEach(id => {
        i = state.selected.indexOf(id)
        if (i > -1) commit('spliceSelected', i)
      })
    }
  },

  extractFilter({ state, getters, dispatch }, text) {
    const filter = jsontkn.extractTokens(text, getters.fieldTokens);
    const isValidFilter = hasProvidedValidFilter(filter, getters.fieldTokens, state.configJsoncnf);
    if (isValidFilter) dispatch('addFilter', filter)
  },
  addFilter({ commit, state }, filter) {
    if (!state.filters.length) filter.logic = "and";
    commit('pushFilter', filter);
  },
  searchForFields({commit, getters}, text) {
    text = text.trim()
    if (text === "") {
      commit('set', {k: 'fieldRecommendations', v: []})
      return
    }
    text = text.split(' ')
    let last = text[text.length-1]
    commit('set', {
      k: 'fieldRecommendations',
      v: getters.fields.filter(f => f.indexOf(last) > -1)}
    )
  }
}

export default actions
