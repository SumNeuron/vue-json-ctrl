import Vue from 'vue'

const mutations = {
  /**
  * set, iset, push and splice have NO error handling. However, they do
  * ensure that Vue refreshes computed properties.
  * So if these are called you better know what you're doing ;)
  */
  set(state, {k, v}) { state[k] = v },
  iset(state, {k, v, i}) { Vue.set(state[k], i, v) },
  push(state, {k, v}) { state[k].push(v); },
  splice(state, {k, i}) { state[k].splice(i, 1); },
  oset(state, {k, obj}) { state[k] = Object.assign({}, state[k], obj); },


  spliceSortSpecification(state, i) {
    state.sortSpecifications.splice(i, 1);
  },

  pushSortSpecification(state, sortSpec) {
    state.sortSpecifications.push(sortSpec);
  },

  setSortSpecification(state, {index, sortSpecification}) {
    state.sortSpecifications.splice(index, 1, sortSpecification);
  },

  pushSelected(state, id) { state.selected.push(id); },
  spliceSelected(state, i) { state.selected.splice(i, 1) },

  pushFilter(state, filter) { state.filters.push(filter); },
  spliceFilter(state, i) { state.filters.splice(i, 1); },
}

export default mutations
