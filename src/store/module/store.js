/**
* import dependencies:
* - jsqlon  - for handling sql-esque JSON
* - jsontkn - for tokenization
* - jsoncnf - for conjunctive normal forms
* - jsontim - for timsort
*/
import jsqlon from 'jsqlon';
import jsontkn from 'json-tkn';
import jsoncnf from 'json-cnf';
import jsontim from 'json-tim';

import state from './state.js'
import getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions.js'

const module = {
  namespaced: true,
  state, mutations, actions, getters
}


export default module
