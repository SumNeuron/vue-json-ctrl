import {config as configJsoncnf} from 'json-cnf';
const state = () => ({
  configJsoncnf: configJsoncnf,

  showRecordId: false,
  hasHeader: true,
  hasFooter: false,
  showFreeText: true,
  showGlobalRegEx: true,
  showTimSort: true,

  isSelectable: false,     // wheter or not records can be selected
  selected: [],            // which records (by id) are selected
  // note: selectability plays no part in the filtering / sorting!!!!


  page: 0,                  // current page
  isPaginated: true,        // whether or not datavue is paginated
  recordsPerPage: 5, // how many records per page to show

  json: {},                 // sql-like JSON
  filters: [],              // a list of filters that defined the conjunctive normal form
  globalRegEx: '',          // a string to search across all fields for a match anywhere
  sortSpecifications: [],    // list of {field, isAscending} objects for how to sort


  fieldRenderFunctions: {}, // a set of functions overwriting how the DataVueRowCell should render
  fieldSortByFunctions: {}, // a set of functions overwriting the default sortby value (json[id][field])
  fieldFilterFunctions: {}, // a set of functions overwriting the default comparision value in filtering (json[id][field])

  fieldOrder: [],           // serves double duty to both set the fields order, as well as which fields to show and use
  fieldRecommendations: []  //
})

export default state
