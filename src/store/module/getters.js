import jsqlon from 'jsqlon';
import jsontkn from 'json-tkn';
import jsoncnf from 'json-cnf';
import jsontim from 'json-tim';
import {identity, defaultFieldRenderFunction, defaultFieldSortByFunction, defaultFieldFilterFunction, lookup} from './utils.js'
const getters = {
  ids: (state) => Object.keys(state.json),
  filter: (state) => (i)  => state.filters[i],
  record: (state) => (id) => state.json[id],
  allFields: (state) => jsqlon.jsonFields(state.json),
  fields: (state, getters) => state.fieldOrder.length === 0 ? getters.allFields : state.fieldOrder,
  isSelected: (state) => (id) => !(state.selected.indexOf(id) < 0),

  // "safely" get the function render/sortby/filter transform function with default to identity
  fieldRenderFunction: (state) => (field) => lookup(state.fieldRenderFunctions, field, defaultFieldRenderFunction),
  fieldSortByFunction: (state) => (field) => lookup(state.fieldSortByFunctions, field, defaultFieldSortByFunction),
  fieldFilterFunction: (state) => (field) => lookup(state.fieldFilterFunctions, field, defaultFieldFilterFunction),

  fieldRender: (state, getters) => (id, field) => getters.fieldRenderFunction(field)(id, field, getters.record(id)),
  fieldSortBy: (state, getters) => (id, field) => getters.fieldSortByFunction(field)(id, field, getters.record(id)),
  fieldFilter: (state, getters) => (id, field) => getters.fieldFilterFunction(field)(id, field, getters.record(id)),

  fieldSortDirection: (state) => (field) => {
    for (let i = 0; i < state.sortSpecifications.length; i++) {
      if (state.sortSpecifications[i].field === field) {
        return state.sortSpecifications[i].isAscending
      }
    }
    return undefined
  },

  // Filter Processing
  // Step 1: filter by CNF
  filtered: (state, getters) => {
    return state.filters.length === 0
      ? getters.ids
      : jsoncnf.filterJSON(state.json, state.filters, state.configJsoncnf);
  },

  // Step 2: filer by global regex
  regmatched: (state, getters) => {
    let ids = getters.filtered
    if (state.globalRegEx === "") return ids
    return jsontkn.giRegexOnFields(
      state.globalRegEx,
      state.json,
      ids,
      getters.fields,
      state.fieldFilterFunctions
    )
  },

  // Step 3: apply tim sort
  timsorted: (state, getters) => {
    return jsontim.timsort(
      state.json,
      state.sortSpecifications,
      getters.regmatched,
      state.fieldSortByFunctions
    )
  },

  // Step 4: take only those for the current page
  paginated: (state, getters) => {
    if ( state.recordsPerPage === Infinity || !state.isPaginated ) return getters.timsorted
    let a = state.page * state.recordsPerPage
    let b = (state.page + 1) * state.recordsPerPage
    return getters.timsorted.slice(a, b)
  },

  fieldValues: (state, getters) => (field) => {
    return getters.paginated.map(id => getters.fieldSortBy(id, field))
  },

  // Other filtering related functions
  /* Can improve performance by a refractor.
  The function filterJSON also calls groupByConjunctiveNormalForm. Here we return
  that result as well as to allow for visualization in the component
  ConjunctiveNormalFormView
  */
  conjunctiveNormalForm: (state) => jsoncnf.groupByConjunctiveNormalForm(state.filters),
  // the token extractor takes first match, so need to sort longest to shortest
  fieldTokens: (state, getters) => jsontkn.sortTokens(getters.fields),

  numberOfRecords: (state, getters) => getters.ids.length,
  numberOfFilteredRecords: (state, getters) => Object.keys(getters.timsorted).length,



  displayConditional: (state) => (conditional) => {
    return state.configJsoncnf.conditionals[conditional].display
  },
  displayFunction: (state) => (func) => {
    return state.configJsoncnf.functions[func].display
  },
  displayLogic: (state) => (logic) => {
    return state.configJsoncnf.logic[logic].display
  }


}

export default getters
